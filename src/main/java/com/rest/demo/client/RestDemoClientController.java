package com.rest.demo.client;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({ "/rest/demo/client" })
public class RestDemoClientController {


		@GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<?> getCall() {
			ClientCall cl = new ClientCall();
			String str = cl.getClientCall();
			return ResponseEntity.status(HttpStatus.OK).body(str);
		}
		
}