package com.rest.demo.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class RestDemoClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestDemoClientApplication.class, args);
	}
}