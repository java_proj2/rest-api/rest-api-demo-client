package com.rest.demo.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ClientCall {

	public String getClientCall(){
		ResponseEntity<String> response = null;
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8095/rest/demo/get";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			//headers.add("admin", "test123");
			HttpEntity<String> request = new HttpEntity<String>(headers);

			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return response.getBody();
	}


}
